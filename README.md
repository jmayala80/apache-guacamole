# Apache Guacamole
 - https://guacamole.apache.org/
 - https://guacamole.apache.org/doc/gug/  
 - https://guacamole.apache.org/doc/gug/guacamole-docker.html

# Initializing the MySQL database
```
$ docker run --rm --name some-mysql \  
  -v ${MYSQL_DATA}:/var/lib/mysql \  
  -e MYSQL_ROOT_PASSWORD=${MYSQL_ROOT_PASSWORD} \  
  -d -p 3306:3306 mysql/mysql-server  

$ docker cp ./mysql/sql-scripts/001-create-schema.sql some-mysql:/  
$ docker cp ./mysql/sql-scripts/002-initdb.sql some-mysql:/  

$ docker exec -it some-mysql bash  

$ cat 001-create-schema.sql | mysql -u root -p  
$ cat 002-initdb.sql | mysql -u root -p guacamole_db  

$ mysql -u root -p  

USE guacamole_db;
SHOW TABLES;
```
# Generate a SQL script  
 - Si su base de datos aún no está inicializada con el esquema de Guacamole, deberá hacerlo antes de usar Guacamole. En la imagen de
   Guacamole se incluye un script de conveniencia para generar el SQL necesario para hacer esto.
```
$ docker run --rm guacamole/guacamole /opt/guacamole/bin/initdb.sh --mysql > 002-initdb.sql 
```
 - Crear tambien el siguiente script (001-create-schema.sql) para la creación de: la base de datos, el usuario y  asignación de privilegios.
```
ALTER USER 'root'@'localhost' IDENTIFIED BY 'passwd';  
CREATE DATABASE guacamole_db;  
CREATE USER 'guacamole_user'@'%' IDENTIFIED BY 'passwd';  
GRANT SELECT,INSERT,UPDATE,DELETE ON guacamole_db.* TO 'guacamole_user'@'%';  
FLUSH PRIVILEGES;  
quit  
```
# Fuente
 - https://techible.net/apache-guacamole-and-nginx-as-reverse-proxy-with-docker/
 - https://www.linode.com/docs/applications/remote-desktop/remote-desktop-using-apache-guacamole-on-docker/
 - https://www.smarthomebeginner.com/install-guacamole-on-docker/
